/**
 *  HS Search
 *  jQuery Search Plugin based on https://github.com/growwithsms/HubSpot-COS-Site-Search
 *
 *  Meant and test for HubSpot but can be used on a different platform
 *
 *  By: Denzel Van Soto
 *  (c) http://denzelsoto.com/
 *
 *  Copright 2017 Denzel Soto
 *  V 1.2
 *
 *
 *  
 *  
 * NOTE!
 * -------------------------------------------------------
 * 
 * To use simply add: $('.yourbtn').hsSearch();
 * 
 * To use with options add:
 * $('.yourbtn').hsSearch({
 *   'highlight' : false
 * });
 * 
 * To use with method add:
 * $('.yourbtn').hsSearch({
 *   'onLoad' : function(){ alert('I'm Working) }
 * });
 *
 *  
 */
(function($) {

    /* ===== Declares the plugin function ===== */
    $.fn.hsSearch = function( options ) {
        /* Declares the options and variables in the plugin */
        var set         = $.extend({

            closeBtnText        : 'x',                  /* Change default close text. Accepts html */
            overlayClick        : true,                 /* Enable/Disable overlay click to close */
            highlight           : true,                 /* Boolean. Enable/Disable text highlighting */
            theme               : false,                /* Use false to remove or the number of theme. */
            noData              : 'No results found',   /* Change default text when no search result */
            sitemap             : '/sitemap.xml',       /* Change Sitemap path */
            searchClass         : '',                   /* Add class to search field */
            customActiveClass   : 'search-activated',   /* Change the added class for the active state on the body tag */
            onLoad              : function() {},        /* Run script after data has been loaded */
            afterComplete       : function() {}         /* Run script after search is complete */
    
        }, options)
            selector    = $(this);
        
        /* Applies the condition to each elements */
        selector.each(function(){

            /* Calls the main function */
            initiateSearch($(this), set);

        });
    }

    /* ===== Declares the main function ===== */
    function initiateSearch(elem, set){
        if(set.theme == 1){
            $('html').addClass('searchTheme');
        }
        
        /* Create and append search elements */
        $('body').append('<div class="search-results-overlay"><div class="search-overlay"></div><button type="button" class="overlay-close">'+set.closeBtnText+'</button><div id="hs-site-search" class="search-results-wrapper"><input class="search '+set.searchClass+'" placeholder="Search" data-list=".search-results.list" /><ul class="search-results list"></ul></div></div>');
        
        /* Close overlay */
        var selector = (set.overlayClick == true)? '.overlay-close, .search-overlay':'.overlay-close';
        $(selector).on('click', function(){
           $('body').removeClass(set.customActiveClass);
            if(set.theme == 1){
                $('html').removeClass('searchStart');
            }
        });

        /* function for capitalizing title */
        String.prototype.toProperCase = function () {
            return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
        };

        function getTitle(url){
            
            url       = url.split("/").reverse().join("/");
            end       = url.indexOf('/');
            title     = url.substr(0, end);

            if(title.length==0){
                url   = url.replace('/', '');
                end   = url.indexOf('/');
                title = url.substr(0, end);
            }
            return title.replace('-',' ').toProperCase();

        }

        /* Ajax get all pages from sitemap.xml */
        $.get(set.sitemap, function(search){
            $(search).find('url').each(function(){
                /* Build individual search result */
                var url     = $(this).find('loc').text(),
                    urlPath = /[^/]*$/.exec(url)[0],
                    name    = urlPath.replace(/[-_]+/g, ' ').replace(/([\.,;])/g, '$1 ').toProperCase(),
                    html    = '<li><a class="search-name" href="' + url + '">' + ((name == '')? getTitle(url):name) + '</a><a class="grayLink" href="' + url + '"><span class="search-url">' + url + '</span></a></li>';
                /* output to DOM */
                $('#hs-site-search ul').append(html);
            });

            /* Run script after data has been loaded */
            set.onLoad.call(this);

            /* Initialize search functionality */
            $('#hs-site-search .search').hideseek({
                highlight: set.highlight,
                nodata   : set.noData
            });
            $('#hs-site-search .search').on("_after", function() {
                /* Run script after search is complete */
                set.afterComplete.call(this);
            });
        });

        /* Check if search is empty on keyup */
        $('#hs-site-search .search').on('keyup', function(e){
            if($(this).val() == ''){
                $('#hs-site-search li').show();
            }
        });

        /* Open search field if search button is click */
        $(elem).on('click', function(e){
            e.preventDefault();
            /* activate modal */
            $('body').toggleClass(set.customActiveClass);
            if(set.theme == 1){
                $('html').addClass('searchStart');
            }
            $('#hs-site-search ul').focus();
        
        });
    }
})(jQuery);




/* =================================== */
/*            Dependencies             */
/* =================================== */

/**
 * HideSeek jQuery plugin
 *
 * @copyright Copyright 2015, Dimitris Krestos
 * @license   Apache License, Version 2.0 (http://www.opensource.org/licenses/apache2.0.php)
 * @link      http://vdw.staytuned.gr
 * @version   v0.7.1
 *
 * Dependencies are include in minified versions at the bottom:
 * 1. Highlight v4 by Johann Burkard
 *
 */
 
!function(e){"use strict";e.fn.hideseek=function(t){var s={list:".hideseek-data",nodata:"",attribute:"text",highlight:!1,ignore:"",headers:"",navigation:!1,ignore_accents:!1,hidden_mode:!1,min_chars:1},t=e.extend(s,t);return this.each(function(){var s=e(this);s.opts=[],e.map(["list","nodata","attribute","highlight","ignore","headers","navigation","ignore_accents","hidden_mode","min_chars"],function(e){s.opts[e]=s.data(e)||t[e]}),s.opts.headers&&(s.opts.ignore+=s.opts.ignore?", "+s.opts.headers:s.opts.headers);var i=e(s.opts.list);s.opts.navigation&&s.attr("autocomplete","off"),s.opts.hidden_mode&&i.children().hide(),s.keyup(function(t){function n(e){return e.children(".selected:visible")}function o(e){return n(e).prevAll(":visible:first")}function a(e){return n(e).nextAll(":visible:first")}if(-1==[38,40,13].indexOf(t.keyCode)&&(8!=t.keyCode?s.val().length>=s.opts.min_chars:!0)){var r=s.val().toLowerCase();i.children(s.opts.ignore.trim()?":not("+s.opts.ignore+")":"").removeClass("selected").each(function(){var t=("text"!=s.opts.attribute?e(this).attr(s.opts.attribute)||"":e(this).text()).toLowerCase(),i=-1==t.removeAccents(s.opts.ignore_accents).indexOf(r)||r===(s.opts.hidden_mode?"":!1);i?e(this).hide():s.opts.highlight?e(this).removeHighlight().highlight(r).show():e(this).show(),s.trigger("_after_each")}),s.opts.nodata&&(i.find(".no-results").remove(),i.children(':not([style*="display: none"])').length||(i.children().first().clone().removeHighlight().addClass("no-results").show().prependTo(s.opts.list).text(s.opts.nodata),s.trigger("_after_nodata"))),s.opts.headers&&e(s.opts.headers,i).each(function(){e(this).nextUntil(s.opts.headers).not('[style*="display: none"],'+s.opts.ignore).length?e(this).show():e(this).hide()}),s.trigger("_after")}s.opts.navigation&&(38==t.keyCode?n(i).length?(o(i).addClass("selected"),n(i).last().removeClass("selected")):i.children(":visible").last().addClass("selected"):40==t.keyCode?n(i).length?(a(i).addClass("selected"),n(i).first().removeClass("selected")):i.children(":visible").first().addClass("selected"):13==t.keyCode&&(n(i).find("a").length?document.location=n(i).find("a").attr("href"):s.val(n(i).text())))})})},e(document).ready(function(){e('[data-toggle="hideseek"]').hideseek()})}(jQuery);


/*
highlight v4

Highlights arbitrary terms.

<http://johannburkard.de/blog/programming/javascript/highlight-javascript-text-higlighting-jquery-plugin.html>

MIT license.

Johann Burkard
<http://johannburkard.de>
<mailto:jb@eaio.com>
*/
jQuery.fn.highlight=function(t){function e(t,i){var n=0;if(3==t.nodeType){var a=t.data.removeAccents(true).toUpperCase().indexOf(i);if(a>=0){var s=document.createElement("mark");s.className="highlight";var r=t.splitText(a);r.splitText(i.length);var o=r.cloneNode(!0);s.appendChild(o),r.parentNode.replaceChild(s,r),n=1}}else if(1==t.nodeType&&t.childNodes&&!/(script|style)/i.test(t.tagName))for(var h=0;h<t.childNodes.length;++h)h+=e(t.childNodes[h],i);return n}return this.length&&t&&t.length?this.each(function(){e(this,t.toUpperCase())}):this},jQuery.fn.removeHighlight=function(){return this.find("mark.highlight").each(function(){with(this.parentNode.firstChild.nodeName,this.parentNode)replaceChild(this.firstChild,this),normalize()}).end()};

// Ignore accents
String.prototype.removeAccents=function(e){return e?this.replace(/[áàãâä]/gi,"a").replace(/[éè¨ê]/gi,"e").replace(/[íìïî]/gi,"i").replace(/[óòöôõ]/gi,"o").replace(/[úùüû]/gi,"u").replace(/[ç]/gi,"c").replace(/[ñ]/gi,"n"):this};
